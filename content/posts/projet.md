---
title: "5 questions avant de se lancer dans une conversion de van."
date: 2020-09-07T10:49:18+02:00
featured_image: "https://www.travel-camper.com/wp-content/uploads/2020/06/location-van-amenage-confort-nantes.jpg"
draft: true
---

## 5 questions à se poser.

Ca y est vous avez l'envie de vous lancer et d'avoir votre propre van, mais
soudain, vous êtes comme pris vertige de la feuille blanche. **Par où
commencer** ?

![let's go](https://media.giphy.com/media/3o7qEbMXvzt9QvePPq/giphy.gif)

Je vais essayer ici de lister les éléments et les différentes questions qui se
sont posées lorsque nous avons essayé de définir le contour de notre projet.

Ceci en espérant que cela puisse vous être utile à votre tour pour vous
permettre d'esquisser le portrait robot de votre futur van.

## 1. Quelle durée de séjour ?

Première question à se poser:

> *Comment vais-je me servir de mon van ?*

Est-ce pour des courts séjours, comme des escapaes le **week end**, ou pour des
séjours de plus longue durée comme **à la semaine** ou des roads trips sur
plusieurs **mois voire années** !

De cette question fondamentale va découler de précieuses informations pour la
rédaction du cahier des charges de votre Van. Il est évident que plus les
séjours seront longs plus il sera nécessaire d'avoir une bonne autonomie en eau
ou en électricité. De plus vous aurez des indications que sur le niveau de
confort souhaitable, douche, wc, etc

## 2. Quelles activités avec mon van ?

Une fois que la question de la durée est posée, il s'agit de savoir quelle sera
le contenu de vos séjours. 

Avez-vous prévu d'aller vous perdre en pleine nature et en autonomie pour
assouvir votre passion comme l'escalade ou le vtt ou plutôt faire des activités
touristiques.

Ces questions doivent vous permettre d'esquisser les grandes lignes concernant
le gabarit de votre futur véhicule ainsi que l'équipement spéficiques relatif à
une potentielle activité sportive.

## 2. De quel confort ai-je besoin ?

Ici, se pose la question de définir votre besoin en terme de confort dans votre
van.

Voici quelques aspects en terme d'équipements à envisager dans votre reflexion, est-ce que vous aurez ve

- chauffage
- douche
- wc
- production de froid: glacière, réfrigérateur voire freezer
- literie permanente ou non
- rangements

## 4. Combien de places ? en terme de couchages et assises

Voici une question assez structurante pour la suite de votre projet, combien de
personnes vont voyager dans votre van ? A minima il vous faudra prévoir autant
de places assises. On peut envisager un nombre de couchages moindre si par
exemple vous avez des enfants qui pourraient utiliser une tente en complément.

Par ailleurs si vous avez un animal de compagnie – bref si vous voyagez avec
Kiki – il vous faudra prévoir un espace pour votre fidèle compagnon. 

## 3. Budget


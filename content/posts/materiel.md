---
title: "Liste de matériel"
date: 2020-09-07T10:49:18+02:00
draft: true
---

## Divers 
- Pince à rivet - [Lien](https://www.amazon.fr/gp/product/B07RPSSBLB/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)
- Sikaflex - [Lien](https://www.amazon.fr/gp/product/B071SJ19R4/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)

## Isolation
- Pic fixation Isolation toit - [Lien](https://www.ebay.fr/itm/62mm-stick-pins-insulation-hangers-Box-of-100/392239613917?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2060353.m2749.l2649)
- Scotch Aluminium - [Lien](https://www.amazon.fr/TiGree-Adh%C3%A9sif-Aluminium-d%C3%A9tanch%C3%A9it%C3%A9-Couleur/dp/B06WD3WRS6/)
- Spay Adhésif Trim Fix - [Lien](https://www.ebay.fr/itm/6x-Trimfix-High-Temp-Spray-Adhesive-500ml-Tins-SPECIAL-OFFER/124136250222?hash=item1ce718db6e:g:9eMAAOSwmLlYBVb5)
- Spray Adhésive + Velour [Gris](https://www.amazon.fr/gp/product/B084Z3CG4J/ref=ox_sc_act_title_1?smid=AW9OSJWZ4RQEE&psc=1) -  [Noir](https://www.amazon.fr/gp/product/B084YTX8BQ/ref=ox_sc_act_title_1?smid=AW9OSJWZ4RQEE&psc=1)

## Gaz
- Aération compartiment à gaz - [Lien](https://www.amazon.fr/PLS-goutte-da%C3%A9ration-Gris-870884/dp/B07DGFK51V)

## Electricité 
- Neutrik PowerCon - [Lien](https://www.amazon.fr/gp/product/B07TLY4CST/ref=ppx_yo_dt_b_asin_title_o00_s01?ie=UTF8&psc=1)

## Aménagement

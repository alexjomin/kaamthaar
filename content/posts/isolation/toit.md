---
title: "Isolation du toit"
date: 2020-10-21T10:27:04+02:00
featured_image: "images/toit/resultat.jpeg"
categories: 
    - isolation
---

L'isolation du toit est une étape indispensable car c'est pas celui-ci que vous
risquez de perdre une bonne partie la chaleurde votre fourgon. Effectivement
l'air chaud ayant tendance à monter, l'air le plus chaud va alors être en
contact avec celui-ci. L'isolation dui toi est donc une étape importante dans le
project d'aménagement. 

<!--more-->

### Liste de matériel

- {{< amazon 3ob9p8i "Ruban adhésif aluminum">}}
- [Pics de maintien de l'isolant](https://www.ebay.fr/itm/392239613917)

### Choix de l'isolant

Comme pour le sol et les murs j'ai opté pour du polyuréthane expansé. Cette fois
avec une **épaisseur de 25mm**. Je trouve ce matériau facile à travailler, de plus
il offre une bonne rigidité et il est recouvert d'un parement fin en aluminum ce
qui permet de faire barrière à l'humdité. 

{{< img "images/toit/isolant.jpeg" >}}

### Fixation

Grâce a l'excellente [chaîne Youtude de Greg Virgoe](https://www.youtube.com/c/GregVirgoe/videos)
j'ai découvert un système de pic adhésif et de goupille afin de maintenir
fermement en place les dalles sur un support.

{{< img "images/toit/pins.jpg" >}}

C'est vraiment génial pour poser les dalles sur un plafond. Ca rend la tâche
vraiment aisée. Une fois la dalle perforée il faut mettre une espèce de goupille
qui maintient l'isolant en place. 

{{< gallery dir="images/toit/gallery/" >}}

### Finition

Une fois les dalles en place on peut assurer une bonne étanchéité avec du ruban
adhésif alumnium. On constitue ainsi une bonne barrière contre l'humdité qui
pourrait provoquer de la condensation sur la tôle du toit.

{{< img "images/toit/resultat.jpeg" >}}

### Conclusion

La mise en oeuvre est tout aussi facile que rapide et le résulat assez
convainquant, je suis vraiment convainqu par cette méthode et je ne peux que
vous la recommander !

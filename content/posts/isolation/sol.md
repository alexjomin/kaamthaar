---
title: "Le Sol du fourgon, isolation et plancher" 
date: 2020-10-11T19:40:03+02:00
featured_image: "/images/sol/sol-cp-peuplier.jpg"
categories: 
    - isolation

---

Le but ici est de réaliser un plancher en contreplaqué, celui-ci va reposer sur
un isolant afin de limiter la dissipation thermique par le sol.

<!--more-->

### Liste de matériel

- {{< amazon 3dqZkz1 "Vis autoforeuses">}}
- {{< amazon 3lvfSc2 "Ruban adhésif aluminium">}}
- {{< amazon 3lxVmr2 "Lamelleuse Einhell">}}
- {{< amazon 30UkTmn "Visseusse Dewalt">}}
- {{< amazon 34RTu5I "Pistolet et bombes de mousse polyuréthane">}}

### Isolation du sol

{{< img "images/sol/dalle-isolant-polyurethane.jpg" "Dalle isolant Polyuréthane" >}}

Pour ma part j'ai choisi de mettre des **dalles de Polyuréthane extrudé de 25mm**.
Notamment car il m'en restait d'un chantier précédent. 

Ce sont des dalles d'environ 1m carré qui sont simples à couper et à mettre en
œuvre. Elles possèdent un système de bouvetage / rainurage afin de les assembler
facilement.

Le coefficient de résistance thermique est de 1. Ce n'est pas énorme mais si on
ne veut pas relever trop le plancher, difficile de faire plus. Le Polyuréthane
extrudé a une bonne résitance mécanique qui fait de ces dalles un bon candidat
pour isoler le sol.

Je ne souhaitais pas utiliser d'isolant mince qui sont très décriés et dont
l'efficacité semblent discutable.

> Le coefficient de résistance thermique est représenté par la lettre R, et est
> exprimé en m². K/W (Kelvin par Watt). Ce coefficient informe donc sur la
> capacité de l'isolant thermique à résister au froid et à la chaleur. Plus la
> résistance thermique R est grande, plus le matériau est isolant.

### Mise en place de l'isolant 

{{< img "images/sol/decoupe-dalle.jpg" "Découpe de Dalle isolant Polyuréthane" >}}

Je me suis servi du plancher déjà existant comme gabarit pour faire les découpes
de l'isolant à l'aide d'un cutter.

{{< img "images/sol/dalles-installees.jpg" "Résultat une fois installée" >}}

Une fois les dalles installées, je les ai solidarisées entres elles avec du
**ruban adhésif aluminium**. J'ai également appliquer ce même ruban entre les
parois et l'isolant afin de garantir un bon hermétisme.

### Mise en place du plancher en contreplaqué 

Pour visser les planches, on utilise des vis autoforeuses qui vont aller percer
la tôle du sol afin de maintenir le panneau de bois. 

J'ai utilisé des vis d'une longueur de 50mm, il faut faire en sorte que la vis
tombe sur un créneau du plancher en tôle.

{{< video src="video/perceuse.mp4" >}}  

Pour le sol, préférez un **contre plaqué de peuplier** de 15mm, il est facile à
travailler et sa masse volumique de 450 kg/m³ vous fera gagner un peu de poid
sur la balance. 

En plus je trouve que le rendu est assez joli pour en faire des meubles. Par
contre c'est un bois tendre qui peut vite prendre des coups.

### Assemblage

Pour assembler vos pièces de bois et garantir un bonne tenue de l'ensemble et
surtout un bon affleurage, vous pouvez utiliser une lamelleuse pour disposer des
"biscuits". Ainsi les deux panneaux seront solidaires et l'affleurage sera
parfait. 

{{< img "images/sol/lamelleuse.jpg" "Lamello" >}}

N'étant pas satisfait avec le jour entre la tôle et le bois à certains endroits
j'ai mis un peu de mousse de Polyuréthane afin de combler l'espace. 

D'ailleurs j'utilise un pistolet avec des cartouches, c'est le jour et la nuit
par rapport à une bombe classique. Il est beaucoup plus facile de doser le
produit.

{{< img "images/sol/mousse.jpg" "Mousse Polyuréthane" >}}

Voici ci-dessous le résultat final du plancher en contreplaqué de 15mm rehaussé
d'un isolant de 25mm. 

{{< img "images/sol/sol-cp-peuplier.jpg" "Résultat final" >}}

> Astuce: Notez que j'ai marqué sur un scotch bleu les endroits haut des
rainures du sol afin de pouvoir visser au bon endroit grâce à un laser ensuite
:)
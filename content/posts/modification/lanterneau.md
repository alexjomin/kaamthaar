---
title: "Pose d'un lanterneau"
date: 2020-10-19T18:24:27+02:00
featured_image: "images/lanterneau/montage-final.jpg"
categories: 
    - modification
---

Un lanterneau permet d'amener de la lumière du jour dans votre fourgon de plus
il assure une ventilation.

La pose peut sembler être une opération délicate, mais si impressionante que
cela puisse paraitre, ce n'est pas si complexe.

<!--more-->

### liste matériel

- {{< amazon 3kd7jCm "Lanterneau Fiama">}}
- {{< amazon 31qGdjZ "Mastic Sika pour camping car">}}
- {{< amazon 3dqZkz1 "Vis autoforeuses">}}
- {{< amazon 3ob9p8i "Ruban adhésif">}}
- {{< amazon 30UkTmn "Visseusse Dewalt">}}
- {{< amazon 34e0hrr "Scie sauteuse Bosch">}}

### Préparation

{{< img "images/lanterneau/cadre.jpg" >}}

Pour solidifier l'ensemble, j'ai commencé par réaliser un cadre en contreplaqué
de peuplier dont les dimensions correspondent à l'ouverture du lanterneau – à
savoir 39x39 cm. 

{{< img "images/lanterneau/preparation.jpg" >}}

Il convient de délimiter l'emplacement de la découpe grâce à de l'adhésif
peinture. En plus de tracer votre découpe, cela protegera votre carroserie de
potentielles traces de scie sauteuse.

{{< notification "info" "ℹ️ Pensez à vérifier plusieurs fois votre côte, ainsi que l'équerrage en mesurant les diagonales, ici on n'a pas vraiment le droit à l'erreur." >}}

### Découpe

Réaliser un trou avec un forêt métal à chaque coin, afin de pouvoir insérer la
lame de la scie sauteuse. Si vous avez du WD40 vous pouvez lubrifier le forêt
afin de faciliter le percage.

{{< video src="video/drilling-roof.mp4" >}}

{{< img "images/lanterneau/decoupe.jpg" >}}

Avec une scie sauteuse dotée d'une lame métal il faut découper la tôle. Vous
verrez cela se passe plutôt bien et la découpe se fait sans trop d'encombres.

Et voilà la découpe est faite !

{{< notification "info" "ℹ️ Pensez à bien nettoyer ou aspirer la limaille produite par votre découpe, sinon elle risque de s'oxyder et produire des tâches de rouille sur le toit !" >}}

### Pose

Comme vous pouvoez le voir le toit n'est pas plat et il y a des créneaux.
Plusieurs techniques s'offrent à vous:

- Combler les créneaux avec du bois (préférez une essence de massif qui pourra
  supporter l'humidité). Une fois ce cadre posé il faudra le noyer, je dis bien
  le noyer de SIKA
- Utiliser uniquement du SIKA, attention vous allez y passer quelques
  cartouches, soyez méticuleux afin de ne laisse aucun espace pour que l'eau
  s'infiltre
- Applatir les créneaux si la surface n'est pas trop conséquente, c'est l'option
  que j'ai choisie, en coupant un peu de matière pour plus facilement applatir
  le métal, j'ai comblé ces trous en mettant du mastic armé. L'opération est un
  peu délicate, mais permet au lanterneau d'etre bien en appui avec le métal
  afin d'assurer une bonne étanchéité. Les pluies du Nord de la france en ce
  mois d'octobre semblent valider la bonne étanchéité de l'ensemble 😀.

Il faut maintenant appliquer de la peinture métal sur la tranche de votre
découpe afin d'éviter un éventuelle oxydation.

Il ne reste plus qu'à appliquer en bonne dose un mastic type SIKA afin d'assurer
l'étanchéité. Puis visser les vis autoforeuses en rajoutant un peu de mastic
dans les trous par mesure de sécurité.

### Résultat

{{< img "images/lanterneau/cadre-dessous.jpg" >}}

Voici la vue de dessous avec le cadre et les vis.  

{{< img "images/lanterneau/pose.jpg" >}}
{{< img "images/lanterneau/montage-final.jpg" >}}

Et voilà le montage est fini !


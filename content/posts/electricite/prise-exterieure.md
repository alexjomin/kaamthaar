---
title: "Prise Exterieure"
date: 2020-10-21T08:45:44+02:00
featured_image: "images/prise/resultat.jpg"
categories: 
    - modification
---

Afin de pouvoir brancher votre fourgon sur une source de courant 220V, vous
pouvez ajouter **une prise P17** qui se logera dans une trappe accessible depuis
l'éxterieur.

<!--more-->

### liste matériel

- {{< amazon 34gPbSz "Trappe Exterieure">}}
- {{< amazon 31qGdjZ "Mastic Sika pour camping car">}}
- {{< amazon 3dqZkz1 "Vis autoforeuses">}}
- {{< amazon 3ob9p8i "Ruban adhésif">}}
- {{< amazon 30UkTmn "Visseusse Dewalt">}}
- {{< amazon 34e0hrr "Scie sauteuse Bosch">}}

### La norme P17 ou CEI 60309

> Les prises P17 correspondent à la norme CEI 60309 est une norme internationale
> définie par la Commission électrotechnique internationale (CEI) pour les
> prises électriques du domaine industriel. Il est notamment utilisé dans les
> ports de plaisance pour les prises électriques de quai. La plus haute tension
> autorisée par ce standard est de 690 V en courant continu ou en courant
> alternatif, la plus grande intensité autorisée de 125 A, et la fréquence la
> plus élevée de 500 Hz, pour une température comprise entre −25 °C et 40 °C1.

{{< img "images/prise/16A-plug.jpg" >}}

> Les différents types de prises prévus par ce standard varient en taille et en
> nombre de broches en fonction des courants autorisés et du nombre de phases du
> prévoit aussi une protection de type IP44, ce qui le préconise pour les usages
> circuit, empêchant une connexion entre circuits incompatibles. Ce standard
> extérieurs à la place du standard de connexion intérieur domestique. 
>
> La prise bleue P+N+T, 6h (180°) est un connecteur simple phase plus
> particulièrement utilisé pour les véhicules de camping et dans les marinas
> d'Europe. La Prise caravane ou Prise de quai a quasi-universellement remplacé
> les autres prises domestiques 230 V du fait de la sécurité IP44 fournie par le
> standard. Quand les socles sont montés avec la connexion orientée vers le bas,
> le système est utilisable en extérieur par tout temps (hors usage maritime)
> avec un IP53. Ce type de connecteur est aussi utilisé en standard pour
> l'éclairage (jusqu'à 16 A) de l'industrie du film et de la télévision
> britannique. 
>
> Source Wikipedia

## Préparation

Comme pour <a href="">la pose du lanterneau</a>, on va  marquer sur un adhésif
type peinture la future découpe. Cet adhésif permettra également de protéger la
peinture de la scie sauteuse.

On réalise un trou à chaque coin afin de pouvoir mettre la lame de la scie
sauteuse.  

{{< img "images/prise/preparation.jpg" >}}

## Découpe

C'est parti on se lance avec la scie sauteuse équipée d'une lame pour métal. 

{{< img "images/prise/decoupe.jpg" >}}
{{< img "images/prise/decoupe-faite.jpg" >}}

{{< notification "info" "ℹ️ On oublie pas mettre de la peinture sur la tranche afin d'éviter l'oxydation et le dêpot de rouille" >}}

## Installation

On nettoie bien la découpe et le contour, puis on dispose une bonne couche de
mastique type SIKA sur le boitier avant de l'insérer de l'insérer. Des vis
autoforeuses sont à installer au 4 coins dans des logements prévus à cet effet. 

{{< img "images/prise/resultat.jpg" >}}

Et voilà !


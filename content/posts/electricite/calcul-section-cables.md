---
title: "Calcul de la section des fils"
date: 2020-10-13T19:07:08+02:00
categories: 
    - electricite
---

### Introduction

Dans votre installation électrique il va falloir dimensionner les fils en
fonction de différents paramêtres. Dans cet article on va prendre le temps de
regarder comment faire les bon choix.

### Câble ou fil ?

L'amalgame est souvent fait mais le fil est un conducteur électrique et le câble
est un groupe de fils ou conducteurs enfermés dans une gaine. Il convient donc
de parler de fils !

### Type de fils

Dans le cadre d'un aménagement de fourgon il convient d'utiliser **un câble
souple** afin d'éviter les cassures éventuelles qu'on pourrait rencontrer avec
du câble rigide.

{{< img "images/electricite/cable-souple.jpg" >}}

### Paramêtres

Intensité
Longueur
Chute de tension

### Sections

La section d'un câble s'exprime en mm² (millimètre carré). Cela correspondant à
la surface du metal conducteur dans le câble, plus connu sous le nom de l'âme.
Les différentes valeurs disponible sur le marché sont normalisées. On va trouver
du 1.5, 2, 5, 4, 6, 10, 16, 25 mm², etc...

Plus le câble est gros plus sa résistivité est faible et plus il pourra faire
transiter un courant elevé sans chauffer.

Un câble sous dimensionné pourra chauffer voire consituer un risque d'incendie.

{{< notification "info" "ℹ️ En cas de valeur non disponible sur le marché, toujours arrondir et prendre la section au dessus !" >}} 

### Un peu de théorie

Voici commennt on calcule la section d'un câble

{{< img "images/electricite/formule.png" >}}

- S: Section en mm²
- ρ: Résistivité du conducteur en Ohm mm² / mètre - Cuivre 0.0178 à 20 degrés
- L: longueur en mètre jusqu'à l'appareil à alimenter (d'où le 2 pour avoir la longueur totale)
- I: Intensité en ampère


- U': Chute de tension relative
- U: Tension en volt
- ΔU = Chute de tension en %

{{< notification "warning" "ℹ️ Détail qui a son importance, la longueur s'entend aller-retour !" >}} 

### Sources

https://www.hoelzle.ch/fr/posts/kabelquerschnitte-und-deren-berechnung
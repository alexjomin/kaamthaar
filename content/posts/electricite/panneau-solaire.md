---
title: "Installation du Panneau Solaire"
date: 2020-10-21T10:27:04+02:00
featured_image: "images/panneau-solaire/resultat.jpg"
categories: 
    - electricite
---

Elements indispensable pour l'autonomie en électricité, le panneau solaire
permettra de recharger votre batterie auxiliare. La pose est assez simple et le
gain en confort réél si on veut ne pas devoir être connecté au réseau électrique
tout le temps.

<!--more-->
### Liste de matériel

- {{< amazon 34RUVm3 "SIKA Colle pour Panneau Solaire" >}}
- {{< amazon 31qGdjZ "SIKA Mastic pour camping car">}}
- {{< amazon 3ob9p8i "Ruban adhésif">}}
- {{< amazon 30UkTmn "Visseusse Dewalt">}}
- {{< amazon 34U6Dg9 "Foret à étage">}}

### Préparation

Il faut tout d'abord trouver un emplacement adapté pour le panneau solaire en
fonction de votre véhicule, de la taille du panneau et des différents équipement
déjà présent sur le toit.  

{{< img "images/panneau-solaire/materiel.jpg" >}}

Une fois l'emplacement trouvé, il faut marqué l'emplacement des supports qui
vont maintenir le panneau. 

{{< img "images/panneau-solaire/marquage.jpg" >}}

Bien penser à nettoyer et dégraisser la surface avant le collage des supports.
Avec mes supports, était fourni un dégraissant dont l'odeur faisait penser a un
détachant type "Eau Ecarlate" afin de bien préparer la surface. 

{{< img "images/panneau-solaire/nettoyage.jpg" >}}

### Installation du panneau

Une fois la surface propre on peut seller les supports avec du SIKA Sealant, ne
pas hésiter à en mettre des bon plots afin que les supports soient parfaitement
sellés au toit. Il faut appuyer fermement le support vers le toit afin de
repartir de façon homogène le SIKA. Il faut penser à vérifier que le support
reste aligné avec la repère. 

{{< img "images/panneau-solaire/sika.jpg" >}}

Une fois les supports mis en place avec le SIKA il faut laisser sécher pendant
24H, pour que le SIKA sêche correcement

{{< img "images/panneau-solaire/sellement-fait.jpg" >}}

On peut alors fixer le panneau sur les supports, je me suis servi ici d'une clé
à cliquet. 

{{< img "images/panneau-solaire/fixation.jpg" >}}

### Installation du passe-câble

{{< img "images/panneau-solaire/percage.jpg" >}}

{{< img "images/panneau-solaire/sika-passe-cable.jpg" >}}
{{< img "images/panneau-solaire/fixation-passe-cable.jpg" >}}

---
title: "Nettoyage et préparation"
date: 2020-10-21T11:00:24+02:00
featured_image: "/images/preparation/IMG_2631.jpeg"
categories: 
    - modification
---

Avant de se lancer dans les travaux une étape est incontournable: **le nettoyage
et la préparation** de votre fourgon.

<!--more-->

### Liste matériel

- {{< amazon 3kjumLI "Alcool Isopropylique">}}
- {{< amazon 2TarSDA "Mastic Armé">}}

## Préparation

L'aménagement précédent à pu laisser des trous dans la carroserie, il faut les
combler avec un {{< amazon 2TarSDA "Mastic Armé">}}.

{{< img "images/preparation/mastic.jpg" >}}

Afin de ne pas en oublier, j'ai préalablemnt réalisé un marquage de tous les
trous, avant d'appliquer le mastic. 

{{< img "images/preparation/trou.jpg" >}} 

C'est un produit facile à appliquer et qui sêche en une vingtaine de minute. 

{{< img "images/preparation/trou-rebouche.jpg" >}}

## Nettoyage

Le but est de déposer l'habillage actuel de votre fourgon afin de le mettre à
nu pour nettoyer l'integralité de la surface et voyez ci-dessous même si le fougon était globalement propre, la crasse arrive à se glisser un peu partout !

{{< gallery dir="images/preparation/gallery/" >}}

Pas de solution miracle ici, l'huile de coude, chiffon et brosse ! Vous pouvez
utiliser de {{< amazon 3kjumLI "L'Alcool Isopropylique">}} pour bien dégraisser
les surfaces. 

{{< img "images/preparation/clean.jpg" >}}

Voici la paroi arrière droite nettoyée. 





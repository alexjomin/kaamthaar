---
title: "Le VASP pour les nuls"
date: 2020-06-20T10:43:54+02:00
draft: true
---

Si vous vous posez la question d'aménager 

Chaque pays membre de l’Union européenne a donc l’obligation de transposer cette directive dans son droit national et c’est ainsi qu’en France, à partir du 20 mai 2018, tous les véhicules de tourismes seront soumis à un contrôle technique renforcé basé sur un référentiel plus précis, qui passera en revue 132 points de contrôle (contre 124 auparavant), mais détaillera surtout 606 défauts potentiels, dont 467 entrainant une contre-visite, contre 178 sur 411 jusqu’alors.


DIRECTIVE  2014/45/UE  DU  PARLEMENT  EUROPÉEN  ET  DU  CONSEIL

>  Non-concordance entre le document d'identification complementaire et le vehicule 


À l’annexe II, point 5.1 de la directive européenne 2007/46/CE relative au cadre de réception communautaire des véhicules [1] sur lequel s’appuie la réforme du contrôle technique nous lisons :
« On entend par « motor-home » (autocaravane) un véhicule à usage spécial de catégorie M conçu pour pouvoir servir de logement et dont le compartiment habitable comprend au moins les équipements suivants :
- des sièges et une table
- des couchettes obtenues en convertissant les sièges
- un coin cuisine
- des espaces de rangement
Ces équipements doivent être inamovibles, toutefois, la table peut être conçue pour être facilement escamotable.  »

Pour faire homologuer un véhicule VASP et obtenir un certificat, afin d’attester la conformité du véhicule par rapport aux réglementations en vigueur :
La première étape consiste à présenter le véhicule à un organisme agréé pour valider sa conformité aux normes NF EN 721 et NF EN 1949. Une fois ce certificat de conformité obtenu, il est nécessaire de présenter le véhicule pour une réception à titre isolée (RTI) auprès de la DREAL (Direction Régionale de l’Environnement, de l’Aménagement et du Logement) ou DRIEE (Direction Régionale et Interdépartementale de l’Environnement et de l’Énergie).
Enfin, une fois l’attestation de réception à titre isolé obtenue, il restera à faire procéder à la modification du certificat d’immatriculation.

[Les texte de lois - Directive Européenne 2007/46/CE](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000697024&categorieLien=id)

| Catégories CE       | Carroserie           | Abbréviations         |
|--------|-----------------------------------|----------|
| M1  | Ambulance (pour personne couchée) | AMBULANC |
| N1, N2 ou N3  | Atelier | ATELIER
| N1, N2 ou N3 	| Bazar forain |BAZ FOR
| N1, N2 ou N3 	| Bennes à ordures ménagères| BOM
| M1 	| Caravane (*)| 	CARAVANE
| N1, N2 ou N3 	| Chariot porteur (9).| CHAR POR
| N1, N2 ou N3 	| Dépannage. | DEPANNAG
| N1, N2 ou N3 	| Fourgon blindé | FG BLIND
| M1 	| Fourgon funéraire | FG FUNER
| N1, N2 ou N3 	| Grue | GRUE
| M1 ou N1 	| Handicapés | HANDICAP
| N1, N2 ou N3 	| Incendie | INCENDIE
| N1, N2 ou N3 	| Magasin | MAGASIN
| M1, N1, N2 ou N3  |	Sanitaire | SANITAIR
| N1, N2 ou N3  |	Travaux publics et industriels.| TRAVAUX
| N1, N2 ou N3  | Voirie | VOIRIE
| M1, N1, N2 ou N3  |	Divers (non spécifiée). | NON SPEC
| M1  | Adaptation réversible dérivée de VP | DERIV VP

(*) Catégories de véhicules pouvant être immatriculés avec un usage véhicule en transit temporaire.

> Source [Legifrance](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020237165)


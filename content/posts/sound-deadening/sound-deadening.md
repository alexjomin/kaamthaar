---
title: "Insonorisation / Sound Deadening"
date: 2020-10-11T21:57:17+02:00
featured_image: "/images/sound-deadening/sound-deadening.jpg"
categories: 
    - isolation
---

Le mot d'insonorisation n'est pas bien adapté, on cherche ici à limiter les
vibrations des parois de tôles – *qui sont de véritables peaux de tambour* – à
l'aide d'adhésif à disposer sur l'intérieur du véhicule. 

<!--more-->

Le phénomène se produit lorsque de l'air est en déplacement contre la
carroserie, il est d'autant plus sensible en cas de pluie, ou pire de grêle, où
les impacts vont faire résonner toute la carroserie.

Cette étape est facultative mais elle vous apportera plus de confort lors de vos
trajets.


## Liste de matériel

- {{< amazon 2Fr8cbu "Dalles Noico 2 mm" >}}

### Le produit

Il existe plusieurs marques sur le marché, j'ai pour ma part choisi ceux de la
marque {{< amazon 2Fr8cbu "Noico" >}}. Le prix était franchement raisonnable, à
savoir un peu moins de 40 euros pour une surface de 1.7 m carré.

Il s'agit de feuilles lestées et adhésives d'une épaisseur de 2mm. Elles sont faciles à
couper à l'aide d'une simple paire de ciseaux. 

Le principe est simple limiter les vibrations sur la tôle afin de limiter le
bruit ainsi produit.

### Application

{{< video src="video/sound-deadening-installation.mp4" >}}

Une fois vos pads découpés à la bonne dimension, la pose est simple, il suffit
de d'appliquer une pression vers la tôle afin de bien coller le produit. Pour ce
faire, j'ai utilisé un jouet de mon fils qui a parfaitement joué son rôle pour
maroufler.

### Test Avant / Après

Mieux qu'une longue explication, voici une petite vidéo afin d'illustrer l'effet
de la pose. On entend bien que les vibrations sont bien atténuées après la mise
en place du {{< amazon 2Fr8cbu "Noico 2 mm" >}}.

{{< video src="video/sound-deadening.mp4" >}}

### Spectrogramme

{{< img "images/sound-deadening/spectrogram.jpg" "Spectrogramme" >}}

J'ai réalisé un petit spectrogramme qui correspond à la vidéo ci-dessus, on voit
nettement que les vibrations qui suivent l'impact s'atténuent beaucoup plus
rapidement quand l'adhésif est mis en place.

### Conclusion

Cette étape même si elle est facultative dans votre projet de conversion de
fourgon ou de van et très rapide à mettre en œuvre avec des résultats vraiment
satisfaisant le tout pour un prix des plus raisonnables ! A mon sens, il serait dommage de
s'en priver.

{{< img "images/sound-deadening/result.jpg" "Résultat finalg" >}}
